import Nav from './Nav';
import {Fragment} from 'react';
import AttendeesList from "./components/AttendeesList.jsx";
import LocationForm from "./components/LocationForm.jsx";
import ConferenceForm from "./components/ConferenceForm.jsx";


function App(props) {
  if (props.attendees === undefined){
    return null;
  }
  return (
    <Fragment><Nav />
    <div className="container">
      { <LocationForm /> }
      { <ConferenceForm /> }
      { <AttendeesList attendees={props.attendees} />}

    </div>
  </ Fragment>
  );
}

export default App;
